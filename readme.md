## Rock Affinity website


# Install #

1. Install flag-icon-css
  * Download https://github.com/lipis/flag-icon-css (zip?)
  * Extract zip, move flag-icon-css-folder to apppath/node_modules/flag-icon-css
1. Create a database
1. Copy .env.example to .env
1. fill-in:
  * Database values
  * Facebook app values
  * Spotify values
  * Google app values
1. composer install
1. npm install
1. php artisan migrate
1. gulp --production