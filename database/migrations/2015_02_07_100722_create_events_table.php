<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('facebook_id')->default('');
			$table->string('google_id')->default('');
			$table->string('name');
			$table->timestamp('start_time');
			$table->timestamp('end_time');
			$table->string('location')->default('');
			$table->string('picture')->default('');
			$table->text('description');
			$table->string('link')->default('');
			$table->integer('people_going')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
