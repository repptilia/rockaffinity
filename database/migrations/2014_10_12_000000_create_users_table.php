<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('facebook_id')->default('');
			$table->string('short_str',20)->default('');
			$table->string('name')->default('');
			$table->string('email')->default('');
			$table->string('password', 60);
			$table->rememberToken();
			$table->string('picture')->default('');
			$table->timestamp('arrived_at');
			$table->timestamp('left_at');
			$table->string('role')->default('');
			$table->text('about_me');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
