<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tracks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('title',100);
			$table->string('artist',100);
			$table->string('cover',100);
			$table->string('spotify_id',25)->default(0);
			$table->string('link');
			$table->integer('popularity')->unsigned()->default(0);
			$table->integer('likes')->unsigned()->default(0);
			$table->integer('duration_ms')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracks');
	}

}
