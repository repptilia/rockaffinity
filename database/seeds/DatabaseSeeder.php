<?php

use Illuminate\Database\Seeder;
use RockAffinity\Models\User;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $this->call('UserTableSeeder');

        $this->command->info('User table seeded!');
	}

}

class UserTableSeeder extends Seeder
{
	public function run()
	{
		User::updateOrCreate([
			'facebook_id' => '10204954763018290'
			],
			[
				'name' => 'Etienne',
				'facebook_id' => '10204954763018290',
				'email' => 'repptilia@gmail.com',
				'picture' => 'https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-9/10322526_767078926658085_5650865926442400305_n.jpg?oh=d5d7321f565c7c40dd80de8693733696&oe=5586919E&__gda__=1434450242_48dc197da0e96924f8818e3c07207e87',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => 'webmaster',
				'password' => str_random(60),
				'about_me' => <<<EOD
*Les études, ça se passe ? qu'est ce que tu fais ?
Je viens de finir mon master en mathématiques! Là, je cherche du travail :)

*T’es vieux comment ?
22 ans

*Tu danses le rock depuis combien de temps ?
Ca fait plus de 2 ans maintenant

*Qu'est ce qui t'as amené à danser ?
Une amie m'a proposé, j'ai tout de suite adoré!

*Les autres danses, ça te parle ? t'as testé ? tu pratiques ?
J'ai fait de la salsa aussi, c'est vraiment cool!

*Et en dehors de la danse ? qu'est que tu fais de ton temps libre ?
Je code, je joue de la guitare, quelques jeux

*Ton meilleur souvenir rock'n'rollesque ? 
Avoir rencontré ma copine <span class="glyphicon glyphicon-heart"></span>

*Ketchup ou mayo ?
Ca dépend des circonstances
*Blonde ou brune ?
Un mélange des deux
*Bière ou vin ?
Vin
*Karmeliet ou Kwak ?
Kwak
*iPhone ou Android ?
iPhone
*Viande ou poisson ?
Viande
*Lac ou montagne ?
Montagne
*Chien ou chat ?
Chat
*Rouge ou blanc ?
Les deux
*Fondue ou raclette ?
Les deux :D
*Migros ou Coop ?
Migros
EOD
,
				'short_str' => 'etienne'
			]);

		User::updateOrCreate([
			'facebook_id' => '741456939241179'
			],
			[
				'name' => 'Mohammed',
				'facebook_id' => '741456939241179',
				'email' => 'mohammed.jerad@epfl.ch',
				'picture' => 'https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-9/10698487_881425731890070_2542577614642908178_n.jpg?oh=a6c2b4016a97453bbbea38ce214832f2&oe=5593B9F5&__gda__=1435419945_548d04c2fd7e3d006ae02a7a88e97f52',
				'arrived_at' => '2014-07-01 00:00:00',
				'role' => 'music',
				'password' => str_random(60),
				'about_me' => '',
				'short_str' => 'mohammed',
			]);

		User::updateOrCreate([
			'facebook_id' => '10152674557883433'
			],
			[
				'name' => 'Amine',
				'facebook_id' => '10152674557883433',
				'email' => 'amine.elamrani@epfl.ch',
				'picture' => 'https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-9/10641134_840408275991816_7556410270197042845_n.jpg?oh=872a313179e57a460fbcf7376499d11e&oe=554E9156&__gda__=1434601276_337aeb397a68638887bf28aa8c89949e',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => 'photoev',
				'password' => str_random(60),
				'about_me' => 'Coucou Etienne :D',
				'short_str' => 'amine',
			]);

		User::updateOrCreate([
			'facebook_id' => '10152737471059182'
			],
			[
				'name' => 'Bernard',
				'facebook_id' => '10152737471059182',
				'email' => '',
				'picture' => 'https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-9/10849805_889600354405941_6111997929975054078_n.jpg?oh=6a12906ab7d54dee2a8b3b7f6456b6b6&oe=55815BEF&__gda__=1435783782_014fbae3dd7216b1036f26bf130b3379',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => 'logist',
				'password' => str_random(60),
				'about_me' => '',
				'short_str' => 'bernard',
			]);

		User::updateOrCreate([
			'facebook_id' => '10205065495699720'
			],
			[
				'name' => 'Stéphane',
				'facebook_id' => '10205065495699720',
				'email' => '',
				'picture' => 'https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-9/10422054_10205442715569981_2009082216204849049_n.jpg?oh=42899ca05a912a72e621c3332f949768&oe=5550BB88&__gda__=1435579383_e95da7a3bbd93257bd60e61f56894ca4',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => 'vicepres',
				'password' => str_random(60),
				'about_me' => "Les études, ça se passe ? qu'est ce que tu fais ? 
Je suis étudiant en Matériaux à l'EFPL. J'ai bientôt terminé mon master mais j’enchaîne sur un doctorat donc je suis encore là pour un bout de temps. 

Tu danses le rock depuis combien de temps ? 
Pas si longtemps que ça, j'ai commencé en septembre (2014). Mais avec une bonne dose de motivation, on peut progresser rapidement ;)

Qu'est ce qui t'as amené à danser ?
La curiosité et l'envie de rencontrer du monde (la danse est le sport social par excellence ). Et pourquoi le rock ? et ben parce que c'est le plus répandu et c'est aussi le style qui me tentait le plus.

Les autres danses, ça te parle ? t'as testé ? tu pratiques ? 
Pour l'instant, je reste très rock'n'roll: 4 temps, 6 temps et acro, il y a déjà de quoi faire. Mais dans les projets, je compte apprendre le Lindy Hop (l'ancêtre du rock donc pas si éloigné que ça) et la salsa probablement.

Et en dehors de la danse ? qu'est que tu fais de ton temps libre ? 
La cuisine. Haha, je suis un grand gourmand et j'adore passer mon temps derrière les fourneaux. Sinon j'aime aussi le volley, le badminton le baby foot et le snowboard. 

Ton meilleur souvenir rock'n'rollesque ? 
Avoir été capable de faire une petite démo de rock acro après seulement 3 mois d'apprentissage.",
				'short_str' => 'stephane',
			]);

		User::updateOrCreate([
			'facebook_id' => '10204199262856185'
			],
			[
				'name' => 'Andrea',
				'facebook_id' => '10204199262856185',
				'email' => 'andrea.polini@epfl.ch',
				'picture' => 'https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-9/10990018_928925300473446_8089323453922430079_n.jpg?oh=8732a067c21a419715781e5b39a28671&oe=557C823D&__gda__=1435844695_f53e0c080af98dc3aebdb7ea5710be12',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => 'president',
				'password' => str_random(60),
				'about_me' => '',
				'short_str' => 'andrea',
			]);

		User::updateOrCreate([
			'facebook_id' => '10153761570022925'
			],
			[
				'name' => 'Marie',
				'facebook_id' => '10153761570022925',
				'email' => '',
				'picture' => 'https://scontent.xx.fbcdn.net/hphotos-xpa1/v/t1.0-9/10676312_858588920840418_8088032753314749182_n.jpg?oh=ed2793e68c8d4c42275094546aab4b0d&oe=55959602',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => 'comphot',
				'password' => str_random(60),
				'about_me' => 'Je m\'engage solennellement à réaliser les meilleures photographies de vos élans artistiques, en capturant l\'instant pour un résultat magique! 
Une envie de danser le 4 temps et tournoyer sans perdre pied? Alors avec moi, vous avez trouvez chaussures à votre pied!',
				'short_str' => 'marie',
			]);

		User::updateOrCreate([
			'facebook_id' => '729672577115548'
			],
			[
				'name' => 'Colombine',
				'facebook_id' => '729672577115548',
				'email' => 'colombine.verzat@epfl.ch',
				'picture' => 'https://scontent.xx.fbcdn.net/hphotos-xpa1/v/t1.0-9/1392996_840409475991696_4728644994431918674_n.jpg?oh=2f9131c7c0b5e943807f865c220d8b33&oe=558B9BE9',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => '',
				'password' => str_random(60),
				'about_me' => '',
				'short_str' => 'colombine',
			]);

		User::updateOrCreate([
			'facebook_id' => '10204716694579974'
			],
			[
				'name' => 'Flavien',
				'facebook_id' => '10204716694579974',
				'email' => '',
				'picture' => 'https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-9/10982139_928927060473270_1260530762825898181_n.jpg?oh=362132df969855954e3c540c35bcdc96&oe=5549AC0F&__gda__=1435016838_c0bb40f0ca5dcc1e6f4bf425b38296e4',
				'arrived_at' => '2013-07-01 00:00:00',
				'role' => 'music',
				'password' => str_random(60),
				'about_me' => <<<EOD
*Les études, ça se passe ? qu'est ce que tu fais ?
Vaut mieux que ça ce passe … Je suis en doctorat. science de la vie ^^
*T’es vieux comment ?
26 X 2 - (9+17)  = mon âge
(oui faut calculer …)
*Tu danses le rock depuis combien de temps ?
J’ai commencé en 2eme année de master, et là je suis en troisième année de doctorat 
(oui je te laisse encore calculer ….)
*Qu'est ce qui t'as amené à danser ?
J’ai vu quelques amis danser, et ils s’éclataient vraiment … je me suis dis que moi aussi.
En plus ils sont super classe quand ils dansent !!!
*Les autres danses, ça te parle ? t'as testé ? tu pratiques ?
Pour l'instant, niveau rock'n'roll: 4 temps, 6 temps et Lindy Hop et un peu de salsa.
Et en dehors de la danse ? qu'est que tu fais de ton temps libre ?
Cuisiner puis … le manger, skier (en profiter pour me péter une clavicule), ne plus skier, slack line, un peu de photo, dessiner, et tous pleins de sports :P
*Ton meilleur souvenir rock'n'rollesque ? 
Pfffffffffffffffff, pleins ça c’est sur !!! J’attends le mercredi avec impatience pour en avoir de nouveaux 
*Blonde ou brune ?
Brune puis blonde … 
*Bière ou vin ?
Bière au bar, vin durant les repas
*Karmeliet ou Kwak ?
Karmeliet 
*iPhone ou Android ?
Android 
*Viande ou poisson ?
Un bon steack !!
*Chien ou chat ?
Chats dernièrement, j’en ai deux ils me font vraiment marrer !!
*Lac ou montagne ?
Lac ET Montagne, on est à Lausanne, on a les deux !!! 
*Rouge ou blanc ?
Rouge
*Fondue ou raclette ?
Rhoo non pas de choix pour ça !! Les deux sont terribles, sans oublier la tartiflette
*Migros ou Coop ?
Celui sur ma route … je m’en fous un peu ^^
EOD
,
				'short_str' => 'flavien',
			]);
	}

}
