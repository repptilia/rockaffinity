<?php namespace RockAffinity\Services;

use RockAffinity\Models\User;

class AuthService
{
	protected $module = 'facebook';

	protected $session;

	public function redirect()
	{
		return redirect($this->getFacebookLoginHelper()->getLoginUrl([
			'scope' => 'manage_pages']));
	}

	public function user()
	{
		$session = $this->getFacebookLoginHelper()->getSessionFromRedirect();

		\Session::put('facebook.token',$session->getToken());

		$request = new \Facebook\FacebookRequest($session, 'GET', '/me/accounts');
		$response = $request->execute()->getGraphObject()->asArray();

		if ( ! array_key_exists('data', $response) )
			return null;

		$canEdit = false;
		foreach ( $response['data'] as $page )
		{
			if ( $page->id === \Config::get('services.facebook.page_id') ){
				$canEdit = true;
				break;
			}
		}

		if ( ! $canEdit )
			return null;

		$request = new \Facebook\FacebookRequest($session, 'GET', '/me');
		$response = $request->execute();

		$user = $this->getRelevantInformationFromFacebook($response->getGraphObject()->asArray());

		$this->stopNativeSession();

		return $user;
	}

	public function getFacebookLoginHelper()
	{
		session_start();
		\Facebook\FacebookSession::setDefaultApplication(\Config::get('services.facebook.client_id'), \Config::get('services.facebook.client_secret'));
		return new \Facebook\FacebookRedirectLoginHelper(\Config::get('services.facebook.redirect'));
	}

	protected function stopNativeSession()
	{
		session_write_close();
	}

	protected function getRelevantInformationFromFacebook(array $data = [])
	{
		return $data;
	}
}