<?php namespace RockAffinity\Services;


class FacebookService
{
	protected $session;

	public function __construct()
	{
		\Facebook\FacebookSession::setDefaultApplication(\Config::get('services.facebook.client_id'), \Config::get('services.facebook.client_secret'));
		
		$this->session = (new \Facebook\FacebookSession(''))->newAppSession(\Config::get('services.facebook.client_id'), \Config::get('services.facebook.client_secret'));
	}

	public function getAlbums()
	{
		$request = new \Facebook\FacebookRequest(
			$this->session,
			'GET',
			'/'.\Config::get('services.facebook.page_id').'/albums'
		);

		$albums = $request->execute()->getGraphObject()->asArray()['data'];

		foreach ( $albums as $key => $album )
		{
			if ( in_array($album->name, ['Profile Pictures','Cover Photos', 'Timeline Photos']))
				unset($albums[$key]);
		}

		return $albums;
	}

	public function getAlbum($id)
	{
		$request = new \Facebook\FacebookRequest(
			$this->session,
			'GET',
			"/$id"
		);

		$album = $request->execute()->getGraphObject()->asArray();

		// Get photos
		$request = new \Facebook\FacebookRequest(
			$this->session,
			'GET',
			"/$id/photos"
		);

		$response = $request->execute();
		$album['data'] = $response->getGraphObject()->asArray()['data'];

		return $album;
	}

	public function getFeed()
	{
		$request = new \Facebook\FacebookRequest(
			$this->session,
			'GET',
			'/'.\Config::get('services.facebook.page_id').'/feed'
		);

		return $request->execute()->getGraphObject()->asArray()['data'];
	}

	public function getEvents()
	{
		$request = new \Facebook\FacebookRequest(
			$this->session,
			'GET',
			'/'.\Config::get('services.facebook.page_id').'/events'
		);

		$events = $request->execute()->getGraphObject()->asArray()['data'];

		foreach ( $events as $key => $event )
		{
			$request = new \Facebook\FacebookRequest(
				$this->session,
				'GET',
				'/'.$event->id
			);
			$events[$key] = $request->execute()->getGraphObject()->asArray();
		}

		return $events;
	}

}