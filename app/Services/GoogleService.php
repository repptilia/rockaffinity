<?php namespace RockAffinity\Services;

use Config;
use Session;

use Google_Client;
use Google_Service_Calendar;
use Google_Auth_AssertionCredentials;

use Carbon\Carbon;

class GoogleService
{
	/**
	 * The google service.
	 *
	 * @var \Google_Service_Calendar
	 */
	protected $calendar;


	public function __construct()
	{
		$client = new Google_Client();
		$client->setApplicationName('RockAffinity');

		$this->calendar = new Google_Service_Calendar($client);

		if ( Session::has('google.serviceToken') )
			$client->setAccessToken(Session::get('google.serviceToken'));

		$credentials = new Google_Auth_AssertionCredentials(
			Config::get('services.google.service_account'),
			['https://www.googleapis.com/auth/calendar'],
			file_get_contents(Config::get('services.google.key'))
			);

		$client->setAssertionCredentials($credentials);

		if ( $client->getAuth()->isAccessTokenExpired() )
			$client->getAuth()->refreshTokenWithAssertion($credentials);

		Session::put('google.serviceToken',$client->getAccessToken());
	}

	public function getComingEvents()
	{
		$events = $this->calendar->events->listEvents(Config::get('services.google.calendar'),[
			'timeMin' => Carbon::now()->toAtomString()])->getItems();

		$raEvents = [];

		foreach ( $events as $event )
		{
			$raEvents[] = [
				'id' => $event->id,
				'name' => $event->summary,
				'start_time' => Carbon::parse($event->start->dateTime)->toDateTimeString(),
				'end_time'   => Carbon::parse($event->end->dateTime)->toDateTimeString(),
				'location'   => $event->location,
				'picture' => '',
				'description' => $event->description?:'',
				'link' => '',
				'people_going' => 0,
				];
		}

		return $raEvents;
	}

}