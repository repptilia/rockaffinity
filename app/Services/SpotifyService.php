<?php namespace RockAffinity\Services;

use Config;
use Session;
use RockAffinity\Models\User;

use SpotifyWebAPI\Session as SpotifySession;
use SpotifyWebAPI\SpotifyWebAPI;

class SpotifyService
{
	protected $api;

	protected $session;

	public function __construct()
	{
		$this->session = new SpotifySession(Config::get('services.spotify.client_id'), Config::get('services.spotify.client_secret'), Config::get('services.spotify.redirect'));
		$this->api = new SpotifyWebAPI();
	}

	public function redirectForAuthentication()
	{
		return redirect($this->session->getAuthorizeUrl([
			'scope' => [
				'playlist-read-private'
				]
			]));
	}

	public function addApiToSessionFromCode($code)
	{
		$this->session->requestToken($code);
		$this->api->setAccessToken($this->session->getAccessToken());
		Session::put('spotify.api',$this->api);
	}
}