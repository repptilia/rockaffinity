<?php namespace RockAffinity\Repositories;

use Mail;
use Config;
use Session;
use Validator;
use RockAffinity\Models\Post;
use RockAffinity\Models\Event;
use RockAffinity\Models\Playlist;
use RockAffinity\Models\Playlist_track;
use RockAffinity\Models\Track;

class MainRepository
{
	public function contactValidator(array $data = [])
	{
		return Validator::make($data,[
			'name' => 'required',
			'email' => 'required|email',
			'message' => 'required'
		]);
	}

	public function sendContactMail(array $data = [])
	{
		Mail::queue('emails.contact',[
			'data' => $data
			],function($message) use ($data){
			$message->to(env('MAIL_USER'))->subject('Contact from '.$data['name']);
		});
	}

	public function lastPosts()
	{
		return Post::orderBy('updated_at','desc')->take(5)->get();
	}

	public function lastEvents()
	{
		return Event::whereRaw('`end_time` > NOW()')->orderBy('start_time')->get();
	}

	public function nextEvent()
	{
		return Event::whereRaw('`end_time` > NOW()')->orderBy('start_time')->first();
	}

	public function getFacebookAlbums()
	{
		$service = new \RockAffinity\Services\FacebookService;

		return $service->getAlbums();
	}

	public function getFacebookAlbum($id)
	{
		$service = new \RockAffinity\Services\FacebookService;

		return $service->getAlbum($id);
	}

	public function getPlaylists()
	{
		if ( ! ( $api = Session::pull('spotify.api') ) )
			return null;

		$offset = 0;
		$limit  = 20;

		do
		{
			$playlists = $api->getUserPlaylists(Config::get('services.spotify.user_id'),[
				'limit' => $limit,
				'offset' => $offset,
				]);

			foreach ( $playlists->items as $playlist )
			{
				if ( $playlist->id )
				{
					$db_playlist = Playlist::updateOrCreate(['spotify_id' => $playlist->id],[
						'name' => $playlist->name,
						'cover' => count($playlist->images)? $playlist->images[0]->url : '',
						'link'  => $playlist->external_urls->spotify,
						'spotify_id' => $playlist->id,
						'count' => $playlist->tracks->total,
						]);

					$track_offset = 0;
					$track_limit  = 100;

					do
					{
						try
						{
							$tracks = $api->getUserPlaylistTracks(Config::get('services.spotify.user_id'),$playlist->id,[
								'limit' => $track_limit,
								'offset' => $track_offset,
								]);

							foreach ( $tracks->items as $track )
							{
								if ( $track->track->id )
								{
									$db_track = Track::updateOrCreate(['spotify_id' => $track->track->id],[
										'title' => $track->track->name,
										'artist' => count($track->track->artists) ? $track->track->artists[0]->name : '',
										'cover' => count($track->track->album->images) ? $track->track->album->images[0]->url : '',
										'spotify_id' => $track->track->id,
										'link' => $track->track->external_urls->spotify,
										'popularity' => $track->track->popularity,
										'duration_ms' => $track->track->duration_ms,
										]);

									Playlist_track::updateOrCreate([
										'track_id' => $db_track->id,
										'playlist_id' =>$db_playlist->id
										],[
										'track_id' => $db_track->id,
										'playlist_id' => $db_playlist->id
										]);	
								}
							}
						}
						catch(\Exception $e)
						{
						}	

						$track_offset += $track_limit;
					}
					while ( $tracks->next );
				}
			}

			$offset += $limit;
		}
		while ( $playlists->next );
	}
}