<?php namespace RockAffinity\Repositories;

use Carbon\Carbon;

use RockAffinity\Models\Post;
use RockAffinity\Services\FacebookService;

class PostRepository
{
	public function getAllFacebookPosts()
	{
		$service = new FacebookService;

		$feed = $service->getFeed();

		foreach ( $feed as $key => $post )
		{
			if ( $post->from->name === 'Rock Affinity' && $post->type !== 'status' && property_exists($post, 'message') )
			{
				Post::updateOrCreate(['facebook_id' => $post->id],[
					'facebook_id' => $post->id,
					'title' => '',
					'message' => $post->message,
					'picture' => property_exists($post, 'picture') ? $post->picture : '',
					'link'    => property_exists($post, 'link') ? $post->link : ''
					]);
			}
		}
	}
}