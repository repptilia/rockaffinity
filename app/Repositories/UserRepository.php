<?php namespace RockAffinity\Repositories;

use Exception;
use RockAffinity\Models\User;

class UserRepository
{
	/**
	 * Find an user.
	 *
	 * @param  string $user
	 * @return User
	 */
	public function findByFacebookIdOrCreate($user)
	{
		if ( ! array_key_exists('id', $user) )
			throw new Exception('invalidData');
			
		$name  = array_key_exists('name', $user) ? $user['name'] : '';
		$email = array_key_exists('email', $user) ? $user['email'] : '';

		if ( ! ($user2 = User::where('facebook_id',$user['id'])->first()) )
			return User::updateOrCreate(['facebook_id' => $user['id']],[
				'facebook_id' => $user['id'],
				'password'  => str_random(60),
				'name' => $name,
				'email' => $email,
				'about_me' => '']);

		else
			return $user2;
	}
}