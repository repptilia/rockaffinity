<?php namespace RockAffinity\Repositories;

use Carbon\Carbon;

use RockAffinity\Models\Event;

class EventRepository
{
	public function getComingEvents()
	{
		$service = new \RockAffinity\Services\FacebookService;

		$events = $service->getEvents();

		foreach ( $events as $event )
		{
			// Determine which values to update
			$data = [
				'facebook_id' => $event['id'],
				'people_going' => 0,
			];

			if ( isset($event['name']) )
				$data['name'] = $event['name'];

			if ( isset($event['start_time']) )
				$data['start_time'] = Carbon::parse($event['start_time'])->toDateTimeString();

			if ( isset($event['end_time']) )
				$data['end_time'] = Carbon::parse($event['end_time'])->toDateTimeString();

			if ( isset($event['location']) )
				$data['location'] = $event['location'];

			if ( isset($event['picture']) )
				$data['picture'] = $event['picture'];

			if ( isset($event['description']) )
				$data['description'] = $event['description'];

			if ( isset($event['link']) )
				$data['link'] = $event['link'];

			if ( isset($event['name']) )
				$data['name'] = $event['name'];

			// If event does not exist already
			if ( ! ($dbEvent = Event::where('start_time',$event['start_time'])->first()) )
				Event::updateOrCreate(['facebook_id' => $event['id']],$data);

			// Else, update already existing event
			else
				Event::updateOrCreate(['id' => $dbEvent->id],$data);
		}

		$events = with(new \RockAffinity\Services\GoogleService)->getComingEvents();

		foreach ( $events as $event )
		{
			if ( ! isset($event['location']) && array_key_exists('location', $event) )
				unset($event['location']);

			// If event does not exist already
			// This ensures that Facebook events have primality; Google events should only be here to indicate; Facebook provides the details
			if ( ! Event::where('start_time',$event['start_time'])->count() )
				Event::updateOrCreate(['google_id' => $event['id']],$event);
		}
	}

	/**
	 * Transforms into JSON an event list (for use with Calendar)
	 *
	 * @param Collection
	 * @return string
	 */
	public function jsonify($events)
	{
		$out = [];

		foreach ( $events as $event )
		{
			$out[] = [
				'id' => $event->id,
				'title' => $event->name,
				'url' => $event->link ?: action('MainController@getEvent',[$event->id,str_slug($event->name)]),
				'start' => 1000*(strtotime($event->start_time)),
				'end' => 1000*(strtotime($event->end_time)),
			];
		}

		return json_encode($out);
	}

}