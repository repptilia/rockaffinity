<?php

if ( ! function_exists('utrans'))
{
	/**
	 * Translate the given message, and makes UCfirst.
	 *
	 * @param  string  $id
	 * @param  array   $parameters
	 * @param  string  $domain
	 * @param  string  $locale
	 * @return string
	 */
	function utrans($id, $parameters = array(), $domain = 'messages', $locale = null)
	{
		return ucfirst(trans($id, $parameters, $domain, $locale));
	}
}