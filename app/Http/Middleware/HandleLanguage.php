<?php namespace RockAffinity\Http\Middleware;

use App;
use Config;
use Closure;
use Session;
use Illuminate\Contracts\Routing\Middleware;

class HandleLanguage implements Middleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		App::setLocale(Session::get('lang',Config::get('lang.default')));

		return $next($request);
	}

}
