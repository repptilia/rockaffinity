<?php namespace RockAffinity\Http\Controllers;

use App;
use Config;
use Session;
use Illuminate\Http\Request;

class LangController extends Controller {

	public function postSetLanguage(Request $request){
		$this->response['success'] = isset(Config::get('lang.list')[$request->input('lg')]);

		if ( $this->response['success'] ){
			App::setLocale($request->input('lg'));

			Session::put('lang',$request->input('lg'));
		}

		return response()->success();
	}
}
