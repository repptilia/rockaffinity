<?php namespace RockAffinity\Http\Controllers\Auth;

use Session;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use RockAffinity\Repositories\UserRepository;
use RockAffinity\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @param  \RockAffinity\Repositories\UserRepository $users
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar, UserRepository $users)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->users = $users;

		$this->middleware('guest', ['except' => ['getLogout','getSpotify']]);
	}

	public function getLogin()
	{
		return $this->getFb(new Request);
	}

	public function getFb(Request $request)
	{
		if ( $request->has('code') )
		{
			$user = (new \RockAffinity\Services\AuthService)->user();

			if ( ! $user )
				return redirect(route('index'))->with('messages.errors',[
					trans('messages.noPermissions')
					]);


			$this->auth->login($this->users->findByFacebookIdOrCreate($user));

			return redirect(action('HomeController@getDashboard'));
		}

		else{
			return (new \RockAffinity\Services\AuthService)->redirect();
		}
	}

	public function getSpotify(Request $request)
	{
		$service = new \RockAffinity\Services\SpotifyService;

		if ( $request->has('code') )
		{
			$service->addApiToSessionFromCode($request->input('code'));

			return redirect(Session::pull('redirectTo'));
		}

		else{
			return $service->redirectForAuthentication();
		}
	}

/*
	public function getGoogle(Request $request)
	{
		if ( $request->has('code') ){
			$user = \Socialize::with('google')->user();

		//	$this->auth->login($this->users->findByEmailOrCreate($user));

			return redirect(route('index'));
		}

		else{
			return \Socialize::with('google')->redirect();
		}
	}*/

}
