<?php namespace RockAffinity\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use RockAffinity\Models\Video;
use RockAffinity\Repositories\MainRepository as Repository;
use RockAffinity\Models\User;
use RockAffinity\Models\Event;

class MainController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	protected $layout = 'layout.main';

	protected $main;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Repository $repository)
	{
		$this->main = $repository;
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return $this->view('page.posts',[
			'posts' => $this->main->lastPosts(),
			'event' => $this->main->nextEvent(),
		]);
	}

	public function getEvents()
	{
		$events = $this->main->lastEvents();

		return $this->view('page.events',[
			'eventsJson' => with(new \RockAffinity\Repositories\EventRepository)->jsonify($events)
		]);
	}

	public function getEventList()
	{
		$events = $this->main->lastEvents();
		return $this->view('page.eventList',[
			'events' => $this->main->lastEvents(),
		]);
	}

	public function getRocker($member)
	{
		if ( $user = User::where('short_str',$member)->first() )
			return $this->view('page.rocker',[
				'user' => $user
			]);

		abort(404);
	}

	public function postRocker($member,Request $request)
	{
		if ( ($user = User::where('short_str',$member)->first()) && $user->email && \Validator::make($request->all(),[
			'message' => 'required'])->passes() )
		{
				\Mail::queue('emails.simple',[
				'data' => $request->input('message')
				],function($message) use($user){
				$message->to($user->email)->subject('RockAffinity website - contact');
			});

			return response()->success($user->email,'messages.contactSent');
		}
	}

	public function getContact()
	{
		return $this->view('page.contact');
	}

	public function postContact(Request $request)
	{
		$validator = $this->main->contactValidator($request->all());

		if ( $validator->fails() )
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$this->main->sendContactMail($request->all());

		return redirect(action('MainController@getContact'))
			->with('messages.valids',[
				trans('messages.contactSent')
			]);
	}

	public function getPartners()
	{
		return $this->view('page.partners');
	}

	public function getVideos()
	{
		return $this->view('page.videos',[
			'videos' => Video::all()
			]);
	}

	public function getPhotos($id = null)
	{
		if ( ! $id )
			return $this->view('page.albums',[
				'albums' => $this->main->getFacebookAlbums()
			]);

		else
			return $this->view('page.photos',[
				'album' => $this->main->getFacebookAlbum($id)
			]);
	}

	public function getAssociation()
	{
		$this->layout = 'layout.full';

		return $this->view('page.association',[
			'users' => \RockAffinity\Models\User::all()
			]);
	}

	public function getPlaylists()
	{
		return $this->view('page.playlists',[
			'playlists' => \RockAffinity\Models\Playlist::all()
			]);
	}

	public function getYourSong()
	{
		return $this->view('page.tracks',[
			'tracks' => \RockAffinity\Models\Track::all()
			]);
	}

	public function getPlaylist($id = null,$name = null)
	{
		if ( ! ( $playlist = \RockAffinity\Models\Playlist::find($id) ) )
			return redirect(route('index'));

		return $this->view('page.playlist',[
			'playlist' => $playlist
			]);
	}

	public function getVoteSong($id)
	{
		if ( Session::has('vote') )
			return redirect(route('index'))->with('messages.valids',[
				'You have voted already!']);

		$t = \RockAffinity\Models\Track::find($id);

		$t->likes = $t->likes + 1;

		$t->save();

		Session::put('vote',true);

		return redirect(route('index'))->with('messages.valids',[
			'Thanks for voting!']);
	}

	/**
	 * Isolated event.
	 *
	 * @param int $id
	 * @param string $slug
	 * @return \Illuminate\Http\Response
	 */
	public function getEvent($id,$slug = null)
	{
		if ( ! ($event = Event::find($id)) )
			return redirect(route('index'));

		if ( ($real_slug = str_slug($event->name)) !== $slug )
			return redirect(action('MainController@getEvent',[$id,$real_slug]));

		return $this->view('event.wide',[
			'event' => $event
			]);
	}

	public function getTest(Request $request)
	{
		throw new \Illuminate\Http\Exception\HttpResponseException(response()->error('invalidQuery'));
	}

}
