<?php namespace RockAffinity\Http\Controllers;

use Auth;
use Session;
use RockAffinity\Models\User;
use RockAffinity\Models\Post;
use RockAffinity\Models\Track;
use RockAffinity\Repositories\MainRepository;

use Illuminate\Http\Request;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	protected $layout = 'layout.user';

	protected $main;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(MainRepository $main)
	{
		$this->main = $main;
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getDashboard()
	{
		return $this->view('page.dashboard');
	}

	public function getCore()
	{
		if ( ! Session::get('spotify.api') )
		{
			Session::put('redirectTo',action('HomeController@getCore'));
			return redirect(action('Auth\AuthController@getSpotify'));
		}

		$this->main->getPlaylists();

		\Artisan::call('ra:maintain');

		return redirect(action('HomeController@getDashboard'))
			->with('messages.valids',[
				trans('messages.spotifyPlaylistsUpdated')
				]);
	}

	public function getProfile($id = null)
	{
		return $this->view('page.profile',[
			'user' => ($id === null) ? Auth::user() : User::findOrFail($id)
			]);
	}

	public function postProfile(Request $request)
	{
		$user = $request->has('user_id') ? User::findOrFail($id) : Auth::user();

		$user->{$request->input('name')} = $request->input('value');

		$user->save();

		return response()->success('',['profileUpdated']);
	}

	public function getEdit($item,$id = null)
	{
		if ( $id === null ){
			\View::share('breadcrumbs',[
					[
						action('HomeController@getEdit',$item),
						ucfirst($item).'s',
					]
				]);
			return $this->view('page.edit.list',[
				'type' => $item,
				'items' => call_user_func('\\RockAffinity\\Models\\'.ucfirst($item).'::all')
				]);
		}

		\View::share('breadcrumbs',[
				[
					action('HomeController@getEdit',$item),
					ucfirst($item).'s',
				],
				[
					action('HomeController@getEdit',[$item,$id]),
					ucfirst($item)." #$id",
				]
			]);
		return $this->view('page.edit.item',[
			'type' => $item,
			'item' => call_user_func_array('\\RockAffinity\\Models\\'.ucfirst($item).'::findOrFail',[$id])
			]);
	}

	public function getMostLikedSongs()
	{
		return $this->view('page.mostLikedSongs',[
			'spotifySongs' => Track::orderBy('popularity','desc')->take(5)->get(),
			'likedSongs'   => Track::orderBy('likes','desc')->take(5)->get()	
			]);
	}

	public function postEdit(Request $request)
	{
		$post = call_user_func_array('\\RockAffinity\\Models\\'.ucfirst($request->input('type')).'::findOrFail',[$request->input('id')]);

		$post->{$request->input('name')} = $request->input('value');

		$post->save();

		return response()->success();
	}

}
