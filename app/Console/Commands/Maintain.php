<?php namespace RockAffinity\Console\Commands;

use RockAffinity\Models\Event;

use Carbon\Carbon;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Maintain extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ra:maintain';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Maintains website content.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Fetch new events
		(new \RockAffinity\Repositories\EventRepository)->getComingEvents();

		// Delete old ones
		Event::where('end_time','<',Carbon::now()->toDateTimeString())->delete();

		// Fetch new posts
		(new \RockAffinity\Repositories\PostRepository)->getAllFacebookPosts();
	}

}
