<?php namespace RockAffinity\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider {

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function($data = null,$messages = [])
        {
            if ( is_array($messages) )
            {
                foreach ( $messages as $key => $item )
                    $messages[$key] = utrans($item);
            }
            else
                $messages = [utrans($messages)];

            return Response::json([
                'success' => true,
                'messages' => [
                    'errors' => [],
                    'valids' => $messages,
                    ],
                'data' => $data
            ]);
        });

        Response::macro('error', function($value)
        {
            if ( is_array($value) )
            {
                foreach ( $value as $key => $item )
                    $value[$key] = utrans($item);
            }
            else
                $value = [utrans($value)];

            return Response::json([
                'success' => false,
                'messages' => [
                    'errors' => $value,
                    'valids' => []
                    ]
            ]);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }

}