<?php namespace RockAffinity\Models;

use Carbon\Carbon;

class Event extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['facebook_id', 'google_id', 'name', 'start_time', 'end_time', 'location', 'picture', 'description', 'link', 'people_going'];

	public function getTimeAttribute()
	{
		$c_start = Carbon::parse($this->start_time);
		$c_end = Carbon::parse($this->end_time);

		if ( $c_start->dayOfWeek === $c_end->dayOfWeek)
			return trans('event.time1',[
				'fday' => utrans("event.day.{$c_start->dayOfWeek}"),
				'day' => $c_start->format('j'),
				'month' => utrans("event.month.{$c_start->format('n')}"),
				'start' => $c_start->format('H:i'),
				'end' => $c_end->format('H:i')]);

		else
			return trans('event.time1',[
				'fday' => utrans("event.day.{$c_start->dayOfWeek}"),
				'day' => $c_start->format('j'),
				'month' => utrans("event.month.{$c_start->format('n')}"),
				'start' => $c_start->format('H:i'),
				'end' => $c_end->format('H:i')]);
	}

}
