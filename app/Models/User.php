<?php namespace RockAffinity\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'facebook_id', 'email', 'password', 'status', 'role', 'picture', 'about_me', 'short_str'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function getProfilePictureAttribute()
	{
		return substr($this->picture, 0,4) === 'http' ? $this->picture : asset("image/team/{$this->picture}");
	}

	/**
	 * Formated about_me
	 *
	 * @return string
	 */
	public function getAboutAttribute()
	{
		$lines = explode(PHP_EOL, $this->about_me);

		foreach ( $lines as &$line )
		{
			if ( ! $line )
				unset($line);

			else if ( $line[0] === '*' )
				$line = '<h4>'.substr($line, 1).'</h4>';
		}
		return implode(PHP_EOL, $lines);
	}
}
