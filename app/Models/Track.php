<?php namespace RockAffinity\Models;

class Track extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'artist', 'cover', 'spotify_id', 'link', 'popularity', 'likes', 'duration_ms'];
	//

	public function getHeartsAttribute()
	{
		return floor(log10($this->popularity)/log10(4));
	}
}
