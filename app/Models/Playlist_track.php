<?php namespace RockAffinity\Models;

class Playlist_track extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['track_id','playlist_id'];

	public $timestamps = false;

}
