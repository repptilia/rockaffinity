<?php namespace RockAffinity\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{

	public function getFillable()
	{
		return $this->fillable;
	}
}
