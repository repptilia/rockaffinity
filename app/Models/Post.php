<?php namespace RockAffinity\Models;

class Post extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['facebook_id', 'title', 'message', 'picture', 'link'];

	public function getTitleAttribute()
	{
		return $this->getAttributeFromArray('title') ?: trans('post.title',['id' => $this->id]);
	}

	public function getMessageAttribute()
	{
		return nl2br(preg_replace('/htt(p|ps)\:\S*/', '<a href="$0">$0</a>', $this->getAttributeFromArray('message')));
	}
}
