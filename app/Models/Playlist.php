<?php namespace RockAffinity\Models;

class Playlist extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['spotify_id', 'name', 'cover','link', 'likes', 'count'];
	//

	public function tracks()
	{
		return $this->belongsToMany('\RockAffinity\Models\Track','playlist_tracks');
	}

}
