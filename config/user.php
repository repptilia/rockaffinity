<?php

return [
	'editables' => [
		'name',
		'picture',
		'email',
		'role',
		'about_me',
		],
	'inputType' => [
		'about_me' => 'textarea'
		]
];