<?php

return [
	'list' => [
		[
			'name' => 'Forum',
			'desc' => 'partner.desc.forum',
			'img'  => 'forum.png',
			'link' => 'http://forum.epfl.ch/',
		],
		[
			'name' => 'Agepoly',
			'desc' => 'partner.desc.agep',
			'img'  => 'ageplogo-petit.jpg',
			'link' => 'http://agepoly.ch'
		],
		[
			'name' => 'DanceSquare',
			'desc' => 'partner.desc.ds',
			'img'  => 'DanceSquare.png',
			'link' => 'http://dancesquare.epfl.ch'
		],
		[
			'name' => 'EPFL',
			'desc' => 'partner.desc.epfl',
			'img'  => 'EPFL.png',
			'link' => 'http://epfl.ch'
		],
		[
			'name' => 'Taco\'s Bar',
			'desc' => 'partner.desc.tacos',
			'img'  => 'tacos-bar.png',
			'link' => 'http://tacos-bar.ch'
		],
		[
			'name' => 'Tech a Break',
			'desc' => 'partner.desc.tech',
			'img'  => 'tech-a-break.png',
			'link' => 'http://www.techabreak.ch'
		],
		[
			'name' => 'Jagger\'s',
			'desc' => 'partner.desc.jaggers',
			'img'  => 'jaggers.png',
			'link' => 'http://www.facebook.com/jaggers-lausanne'
		],
		[
			'name' => 'Ecole de danse Valentin Meier',
			'desc' => 'partner.desc.meier',
			'img'  => 'valentin-meier.png',
			'link' => 'http://www.danse-lausanne.ch'
		]
	],
	'side' => [
		[
			'img' => 'forum.png',
			'link' => 'http://forum.epfl.ch'
		],
		[
			'img' => 'tacos-bar.png',
			'link' => 'http://tacos-bar.ch'
		],
		[
			'img'  => 'tech-a-break.png',
			'link' => 'http://www.techabreak.ch/',
		],
		[
			'img' => 'jaggers.png',
			'link' => 'http://www.facebook.com/jaggers-lausanne'
		],
		[
			'img' => 'valentin-meier.png',
			'link' => 'http://www.dancelausanne.ch'
		]
	]
];