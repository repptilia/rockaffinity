<?php

return [
	'list' => [
		'fr' => 'french',
		'en' => 'english'
	],
	'default' => 'fr'
];