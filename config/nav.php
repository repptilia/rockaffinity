<?php

return [
	'list' => [
		'news' => 'MainController@getIndex',
		'events' => 'MainController@getEvents',
		'association' => 'MainController@getAssociation',
		'links' => [
			'partners' => 'MainController@getPartners',
			'videos' => 'MainController@getVideos',
			'photos' => 'MainController@getPhotos',
		],
		'contact' => 'MainController@getContact',
		'music' => [
			'yourSong' => 'MainController@getYourSong',
			'playlists' => 'MainController@getPlaylists']
	]
];