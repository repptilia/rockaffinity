<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

	'facebook' => [
		'client_id'     => '249847935176010',
		'client_secret' => env('FB_SECRET'),
		'redirect'      => env('FB_REDIRECT'),
		'page_id'       => '281266561905993',
	],

	'spotify' => [
		'client_id' => '47c7e195dc5341cb9de56be47d4e1d26',
		'client_secret' => env('SP_SECRET'),
		'redirect'      => env('SP_REDIRECT'),
		'user_id'       => '11128476863',
	],

	'google' => [
		'client_id'       => env('GO_CLIENT_ID'),
		'service_account' => env('GO_SERVICE_ACCOUNT'),
		'calendar'        => 'rockaffinity.epfl@gmail.com',
		'key'             => base_path().'/key.p12', // NOTE: Changing this requires a change in .gitignore
	],

];
