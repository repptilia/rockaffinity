var elixir = require('laravel-elixir');
var gulp = require('gulp');
var shell = require('gulp-shell');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task('compile:icomoon', shell.task([
	'node_modules/.bin/icomoon-build -p resources/assets/json/ChillingMonkeyIcomoon.json --less resources/assets/less/icomoon.auto.less --fonts resources/fonts'
]));

elixir(function(mix){
	mix.less('ra.less')
		.scripts([
			'../../../node_modules/bootstrap/dist/js/bootstrap.js',
			'../../../node_modules/bootstrap-validator/js/validator.js',
			'../../../node_modules/underscore/underscore.js',
			'../../../node_modules/jstimezonedetect/jstz.js',
			'../../../node_modules/bootstrap-calendar/js/calendar.js',
			'../../../node_modules/bootstrap-calendar/js/language/fr-FR.js',
			'main.js',
		],'public/js/ra.js')
		.version([
			'css/app.css',
			'js/ra.js'
		])
		.copy('node_modules/bootstrap/fonts','public/build/fonts')
		.copy('node_modules/bootstrap-calendar/img','public/build/img')
		.copy('node_modules/bootstrap-calendar/tmpls','public/tmpls')
		.copy('node_modules/flag-icon-css/flags', 'public/build/flags');
});