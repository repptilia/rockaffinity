<?php

return [
	'feedback' => 'feedback',
	'mostLikedSongs' => 'most liked songs',
	'manageProfile' => 'manage profile',
	'manageContent' => 'manage content',
];