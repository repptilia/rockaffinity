<?php

return [
	'welcome' => 'welcome, :name!',
	'dashboard' => 'dashboard',
	'profile' => 'my profile',

	'name' => 'name',
	'picture' => 'picture (URL)',
	'role' => 'role',
];