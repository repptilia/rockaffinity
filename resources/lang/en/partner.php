<?php

return [
	'site' => 'their site',
	'desc' => [
		'agep'    => "It's the student association of the EPFL without whom we wouldn't exist ! We thank them ! Aside from that, they do plenty of cool stuff, check them out !",
		'ds'      => "Kudos for our commission! Created in the same time frame as us, it still grooves timidly and awaits on people that have time for it !",
		'epfl'    => "Great school and great sponsor ! :D",
		'tacos'   => "It's thanks to them that we can dance. You are welcome there anytime !",
		'jaggers' => "An amazing nightclub to practice your rock every night, especially during our dancing nights!",
		'meier'   => "Dancing school specialized in ballroom dance, which generously sponsorizes us.",
	]
];