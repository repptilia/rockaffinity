<?php

return [
	'team' => 'the team',
	'title' => [
		'who' => 'Who are we ?',
		'why' => 'Why rock ?',
		'for' => 'For who ?'
	],
	'text' => [
		'who' => [
			'Rock Affinity belongs to the Dance Square commission affiliated with the AGEPoly. It was created recently so Rock Affinity is still in a development phase.',
			'Its goal is to share and spread rock dancing among the students.',
			'To achieve it, Rock Affinity organizes several events throughout the year: weekly dancing evenings, initiatic lessons or even special events.'
		],
		'why' => [
			'The two founders were dance partners and met at the UNIL Centre de Sport during the rock course. We quickly noticed our common passion and wanted to find another place to dance. But it wasn\'t that easy. In fact, in Lausanne, there were only sporadic events quite spread from each other. By talking around us, we noticed that we were not the only ones interested by such a place. With that in mind, we decided to create Rock Affinity in order to provide for the expectations of these persons. To help in our decisions, we take example on the rock associations in France. There are a lot of them and they have a great success.'
		],
		'for' => [
			'The evenings are open to everyone and every level, beginners and advanced. They focus on three major types of rock dance:',
			"The 4 time signature rock (rock à 4 temps), also called free rock or rally. Very spread out among the french students associations. It's easy and fast to learn, it's praised by beginners.",
			"The 6 time signature rock (rock à 6 temps), also called jive. It's one of the two styles of rock taught by the UNIL-EPFL course. It's also one of the most spread out in Europe.",
			"The \"jump rock\" (rock sauté). It's the second style taught in the UNIL-EPFL course, in the advanced course.
The goal of these dancing evenings is really to share our passion for rock dance with a maximum of people. For this, our members and ourselves are available for those who want advice or to learn.",
		]
	],
];