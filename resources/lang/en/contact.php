<?php

return [
	'where' => 'where are we?',
	'contact' => 'contact us!',
	'name' => 'name',
	'email' => 'email',
	'object' => 'object',
	'message' => 'message',
	'send' => 'send'
];