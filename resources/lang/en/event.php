<?php

return [
	'time1' => ':day :date, from :start to :end',
	'time2' => 'from :starth to :end',

	'day' => [
		1 => 'monday',
		2 => 'tuesday',
		3 => 'wednesday',
		4 => 'thursday',
		5 => 'friday',
		6 => 'saturday',
		7 => 'sunday'
	]
];