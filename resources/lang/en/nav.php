<?php

return [
	'news' => 'news',
	'association' => 'association',
	'links' => 'links',
	'contact' => 'contact',
	'music' => 'music',
	'music.yourSong' => 'yourSong',
	'music.playlists' => 'playlists',
	'links.partners' => 'partners',
	'links.videos' => 'videos',
	'links.photos' => 'photos',
];