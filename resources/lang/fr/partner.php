<?php

return [
	'site' => 'leur site',
	'desc' => [
		'agep'    => "C'est l'association des étudiants de l'EPFL sans lequel nous n'existerons pas ! Merci à eux ! A côté de ça ils font plein de truc cool. Allez les voir !",
		'ds'      => "la petit coup de pub pour notre commission mère. Née en même temps que nous, elle se trémousse encore timidement et attend des gens motivés qui ont du temps pour elle !",
		'epfl'    => "Super école ! Et bon sponsor !",
		'tacos'   => "Big up au Taco's ! C'est chez eux que ça se passe, c'est eux qui nous permettent de vous faire danser ;)",
		'jaggers' => "Une super boîte, pour pratiquer la danse tous les soirs de la semaine, en particulier lors de nos soirées!",
		'meier'   => "Ecole spécialiste en danse de salon et en swing, elle nous sponsorise généreusement.",
		'tech'    => "Un super bar, au quartier nord de l'EPFL! On y fait des super soirées, venez goûter leurs burgers!",
		'forum'   => "Qui ne connaît pas le forum? Super sponsor!!!"
	]
];