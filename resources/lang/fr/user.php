<?php

return [
	'welcome' => 'bienvenue, :name!',
	'dashboard' => 'dashboard',
	'profile' => 'mon profil',

	'name' => 'nom',
	'picture' => 'photo (URL)',
	'role' => 'rôle',
	'about_me' => 'sur moi',
	'email' => 'e-mail',

	'contactMe' => 'contacte moi!'
];