<?php

return [
	'feedback' => 'feedback',
	'mostLikedSongs' => 'chansons préférées',
	'manageProfile' => 'gestion profil',
	'manageContent' => 'gestion contenu',
];