<?php

return [
	'president' => 'président',
	'webmaster' => 'webmaster, design',
	'comm'      => 'communication',
	'vicepres'  => 'vice président',
	'logist'    => 'logistique, comptabilité',
	'music'     => 'musique',
	'secret'    => 'secrétaire',
	'companim'  => 'communication, animation',
	'photoev'   => 'photographie, évènements',
	'logistique'=> 'logistique, affichage'
];