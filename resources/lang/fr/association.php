<?php

return [
	'team' => 'l\'équipe',
	'title' => [
		'who' => 'Qui sommes-nous ?',
		'why' => 'Pourquoi le rock ?',
		'for' => 'Pour qui ?'
	],
	'text' => [
		'who' => [
			"Rock Affinity est une sous-commission de DanceSquare, commission de l'AGEPoly.",
			"Son objectif est de partager et de répandre la danse rock chez les étudiants.",
			"Pour ce faire, Rock Affinity organise différents événements tout au long de l’année, allant de soirées hebdomadaires, aux cours d’initiation en passant par des évènements particuliers.",
		],
		'why' => [
			"Les fondateurs se sont rencontrés aux cours de rock organisés au centre sportif de l'UNIL, où ils ont été partenaires.",
			"Ils se sont alors découverts une passion commnue qui leur ont donné envie de trouver un autre endroit pour danser le rock, ce qui s'est avéré difficile sur Lausanne : seuls quelques sporadiques évènements permettaient de se défouler sur les pistes.",
			"En en parlant autour d'eux, ils se sont rendus compte que beaucoup d'autres personnes cherchaient un tel endroit.",
			"C'est alors que Rock Affinity a été créée, afin de répondre à cette demande et de retrouver d'autres passionnés de la dance.",
		],
		'for' => [
			"Les soirées sont libres d’accès pour tout le monde, autant les débutants que les avancés. Elles centrent trois types majeurs de rock :",
			"Le rock à 4 temps, nommé aussi rock libre ou de rally, très répandu dans les associations étudiantes françaises. Facile et rapide à apprendre, il est souvent du goût des débutants.",
			"Le rock à 6 temps, nommée aussi Jive. C’est un des deux styles de rock enseigné par l’UNIL-EPFL. C’est aussi l’un des plus rependus à travers l’Europe.",
			"Le rock sauté. C’est le deuxième style enseigné par l’UNIL-EPFL au cours de niveau avancé.",
			"L’objectif de ces soirées est vraiment de partager notre passion du rock et de toucher un maximum de personnes . Pour cela, nos membres et nous-mêmes sommes à disposition de ceux qui veulent être conseillés que ce soit dans l’apprentissage ou l’approfondissement.",
		]
	],
];