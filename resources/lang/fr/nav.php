<?php

return [
	'news' => 'annonces',
	'events' => 'evènements',
	'association' => 'l\'association',
	'links' => 'liens',
	'contact' => 'contact',
	'music' => 'musique',
	'music.yourSong' => 'ta chanson!',
	'music.playlists' => 'playlists',
	'links.partners' => 'partnenaires',
	'links.videos' => 'vidéos',
	'links.photos' => 'photos',
];