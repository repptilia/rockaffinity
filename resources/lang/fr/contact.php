<?php

return [
	'where' => 'où sommes-nous?',
	'contact' => 'contactez-nous!',
	'name' => 'nom',
	'email' => 'e-mail',
	'object' => 'objet',
	'message' => 'message',
	'send' => 'envoyer'
];