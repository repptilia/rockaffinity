@section('page')
<p class="lead">{{$user->name}} : {{utrans("role.{$user->role}")}}</p>
	<div class="col-md-6 pull-right">
		<img src="{{asset($user->picture)}}" alt="" class="img-responsive" style="border:10px dotted #000" />
	</div>
{!!$user->about!!}

@if ( $user->email )
	<br />
	<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#ra_contactM"><span class="glyphicon glyphicon-envelope"></span> {{utrans('user.contactMe')}}</button>
	<div class="modal fade" id="ra_contactM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ra_contactM_label"><span class="glyphicon glyphicon-envelope"></span> {{utrans('user.contactMe')}}</h4>
				</div>
				<div class="modal-body">
					<textarea name="message" class="form-control" id="ra_contact_msg"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="ra_contact_me">{{utrans('user.contactMe')}}</button>
				</div>
			</div>
		</div>
	</div>
@endif
<hr />
<a href="{{action('MainController@getAssociation')}}" class="lead"><span class="glyphicon glyphicon-plus"></span> {{utrans('association.team')}}</a>
@stop