@section('page')
<p class="lead">{{utrans('main.feedback')}}</p>
<ul>
	<li><a href="{{action('HomeController@getMostLikedSongs')}}">{{utrans('main.mostLikedSongs')}}</a></li>
</ul>
<p class="lead">{{utrans('main.manageProfile')}}</p>
<ul>
	<li><a href="{{action('HomeController@getProfile')}}">{{utrans('user.profile')}}</a></li>
</ul>
<p class="lead">{{utrans('main.manageContent')}}</p>
<ul>
	<li><a href="{{action('HomeController@getEdit','post')}}">{{utrans('main.posts')}}</a></li>
	<li><a href="{{action('HomeController@getEdit','event')}}">{{utrans('main.events')}}</a></li>
	<li><a href="{{action('HomeController@getCore')}}">{{utrans('main.updateBase')}}</a></li>
</ul>
@stop