@section('page')
<div class="ra_img_cont">
	<img src="{{asset('image/team.jpeg')}}" class="img-responsive" alt="" style="border:10px dotted #000"/>
	<a href="{{action('MainController@getRocker','etienne')}}" class="ra_member_on_image" style="top:105px;left:654px"></a>
	<a href="{{action('MainController@getRocker','andrea')}}" class="ra_member_on_image" style="top:160px;left:570px"></a>
	<a href="{{action('MainController@getRocker','mohammed')}}" class="ra_member_on_image" style="top:180px;left:310px"></a>
	<a href="{{action('MainController@getRocker','bernard')}}" class="ra_member_on_image" style="top:160px;left:510px"></a>
	<a href="{{action('MainController@getRocker','stephane')}}" class="ra_member_on_image" style="top:110px;left:350px"></a>
	<a href="{{action('MainController@getRocker','marie')}}" class="ra_member_on_image" style="top:180px;left:380px"></a>
	<a href="{{action('MainController@getRocker','amine')}}" class="ra_member_on_image" style="top:140px;left:750px"></a>
	<a href="{{action('MainController@getRocker','flavien')}}" class="ra_member_on_image" style="top:164px;left:208px"></a>
	<a href="{{action('MainController@getRocker','colombine')}}" class="ra_member_on_image" style="top:170px;left:450px"></a>
</div>
<p class="lead">{{utrans('association.team')}}</p>
<div class="row">
@foreach ( $users as $user )
	<div class="col-md-4" style="margin-bottom:5px">
		<a href="{{action('MainController@getRocker',$user->short_str)}}" class="ra_member_text">
		<div class="media">
			<div class="media-left">
					<img class="media-object" style="width:64px;height:64px" src="{{$user->profile_picture}}" alt="" />
			</div>
			<div class="media-body">
				<h4 class="media-heading">{{$user->name}}</h4>
					{{utrans("role.{$user->role}")}}
			</div>
		</div>
		</a>
	</div>
@endforeach
</div>
<hr />
<p class="lead">{{utrans('association.title.who')}}</p>
<div class="row">
		<div class="col-md-3">
			<img class="img-responsive" src="{{asset('image/what.png')}}" alt="" />
		</div>
		<div class="col-md-9">
@foreach ( trans('association.text.who') as $item )
			<p>{{ucfirst($item)}}</p>
@endforeach
		</div>
</div>
<hr />
<p class="lead">{{utrans('association.title.why')}}</p>
<div class="row">
		<div class="col-md-9">
@foreach ( trans('association.text.why') as $item )
			<p>{{ucfirst($item)}}</p>
@endforeach
		</div>
		<div class="col-md-3">
			<img class="img-responsive" src="{{asset('image/guy.png')}}" alt="" />
		</div>
</div>
<hr />
<p class="lead">{{utrans('association.title.for')}}</p>
<div class="row">
		<div class="col-md-3">
			<img class="img-responsive" src="{{asset('image/girl.png')}}" alt="" />
		</div>
		<div class="col-md-9">
@foreach ( trans('association.text.for') as $item )
			<p>{{ucfirst($item)}}</p>
@endforeach
		</div>
</div>
<hr />
@stop