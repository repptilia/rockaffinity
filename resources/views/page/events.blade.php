@section('page')

<p class="lead">{{utrans('event.calendar')}}<a href="{{action('MainController@getEventList')}}" class="pull-right">{{utrans('event.list')}}</a></p>

{{utrans('event.chooseMonth')}}
<div class="btn-group btn-group-sm">
	<button href="#" class="btn btn-default" data-calendar-nav="prev">{{utrans('event.prevC')}}</button>
	{{--<button class="btn" data-calendar-nav="today">Today</button>--}}
	<a href="#" class="btn btn-primary"><span id="ra_cal_title"></span></a>
	<button class="btn btn-default" data-calendar-nav="next">{{utrans('event.nextC')}}</button>
</div>

<div id="ra_calendar"></div>

<div id="ra_calendar_data" class="hidden">
{!!$eventsJson!!}
</div>

@stop