@section('page')
<p class="lead">{{utrans('playlist.yourSong')}}</p>
<div class="row">
@foreach ( $tracks as $track )
	<div class="col-sm-3 col-xs-6" style="text-align:center;">
@if ( ! Session::has('vote') )
		<a href="{{action('MainController@getVoteSong',$track->id)}}">
@endif
			<span class="text-align:center">
@for ( $i=0; $i < $track->hearts ; $i ++ )
<span class="glyphicon glyphicon-heart"></span>
@endfor
			</span><br />
			<img src="{{$track->cover}}" alt="" class="img-thumbnail" style="height:100px;width:100px" />
			<p style="white-space:nowrap;overflow:hidden;text-overflow:ellipsis;">{{$track->title}} - {{$track->artist}}</p>
@if ( ! Session::has('vote') )
		</a>
@endif
	</div>
@endforeach
</div>
@stop