@section('page')

<p class="lead">{{utrans('event.list')}} <a href="{{action('MainController@getEvents')}}" class="pull-right">{{utrans('event.calendar')}}</a></p>

@foreach ( $events as $event )
@include('event.inList')
<hr />
@endforeach
@stop