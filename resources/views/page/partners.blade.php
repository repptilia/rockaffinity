@section('page')
@foreach ( Config::get('partners.list',[]) as $key => $partner )
<p class="lead">{{$partner['name']}}</p>
<div class="row">
	@if ( $key % 2 )
		<div class="col-xs-3">
			<a href="{{$partner['link']}}"><img class="img-responsive" src="{{asset('image/sp/'.$partner['img'])}}" alt="" /></a>
		</div>
		<div class="col-xs-9">
			<p>{{utrans($partner['desc'])}}</p>
			<p><a href="{{$partner['link']}}">{{utrans('partner.site')}}</a></p>
		</div>
	@else
		<div class="col-xs-9">
			<p>{{utrans($partner['desc'])}}</p>
			<p><a href="{{$partner['link']}}">{{utrans('partner.site')}}</a></p>
		</div>
		<div class="col-xs-3">
			<a href="{{$partner['link']}}"><img class="img-responsive" src="{{asset('image/sp/'.$partner['img'])}}" alt="" /></a>
		</div>
	@endif
</div>
<hr />
@endforeach
@stop