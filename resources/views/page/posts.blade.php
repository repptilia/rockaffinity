@section('page')

<h1>{{utrans('event.next')}}</h1>
<div class="alert alert-danger">
@if ($event)
	@include('event.inList')
@endif
</div>
<hr />
@foreach ( $posts as $post )
<div class="media">
	<div class="media-left">
		<img class="media-object ra_post_picture" src="{{$post->picture}}" alt="">
  </div>
  <div class="media-body">
    <h4 class="media-heading">{{$post->title}}</h4>
    {!!$post->message!!}
  </div>
</div>
<hr />
@endforeach
@stop