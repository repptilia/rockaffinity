@section('page')

<p class="lead">{{utrans('user.profile')}}</p>
<form class="form-horizontal ra_profile_edit" data-id="{{$user->id}}">
@foreach ( Config::get('user.editables',[]) as $name )
	<div class="form-group">
		<label for="ra_edit_{{$name}}" class="col-sm-2 control-label">{{utrans("user.$name")}}</label>
		<div class="col-sm-10">
	@if ( Config::has("user.inputType.$name") )
		@if ( Config::get("user.inputType.$name") === 'textarea' )
			<textarea name="{{$name}}" class="form-control" data-name="{{$name}}" id="ra_edit_{{$name}}">{{$user->$name}}</textarea>
		@endif
	@else
			<input type="text" class="form-control" id="ra_edit_{{$name}}" data-name="{{$name}}" value="{{$user->$name}}" />
	@endif
		</div>
	</div>
@endforeach
</form>
@stop