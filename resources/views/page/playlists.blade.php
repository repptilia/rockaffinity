@section('page')
<p class="lead">{{utrans('playlist.list')}}</p>
<div class="row">
@foreach ( $playlists as $playlist )
	<div class="col-sm-3 col-xs-6" style="text-align:center;">
		<a href="{{action('MainController@getPlaylist',$playlist->id)}}">
			<img src="{{$playlist->cover}}" alt="" class="img-thumbnail" style="height:100px;width:100px" />
			<p style="white-space:nowrap;overflow:hidden;text-overflow:ellipsis;">{{$playlist->name}}</p>
		</a>
	</div>
@endforeach
</div>
@stop