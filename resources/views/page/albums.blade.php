@section('page')
<p class="lead">{{utrans('album.list')}}</p>
<div class="row">
@foreach ( $albums as $album )
	<div class="col-sm-3 col-xs-6" style="text-align:center;">
		<a href="{{action('MainController@getPhotos',$album->id)}}">
			<img src="http://graph.facebook.com/{{$album->cover_photo}}/picture?type=thumbnail" alt="" class="img-thumbnail" style="height:100px;width:100px" />
			<p style="white-space:nowrap;overflow:hidden;text-overflow:ellipsis;">{{$album->name}}</p>
		</a>
	</div>
@endforeach
</div>
@stop