@section('page')
<p class="lead">{{utrans('contact.where')}}</p>

<iframe style="width:100%;border:0" height="300" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA6KyHsATuHdj2ufLXz8P5q03OOcaBeqR8&q=Tacos%20Bar%20Lausanne"></iframe>

<p class="lead">{{utrans('contact.contact')}}</p>
<div class="row">
	<div class="col-md-8 col-sm-8 col-xs-12">
		<form action="{{action('MainController@postContact')}}" method="post" accept-charset="utf-8" class="form-horizontal cm_contact_form">
			<div class="form-group">
				<label for="" class="col-md-3 control-label">{{utrans('contact.name')}}<sup>*</sup></label>
				<div class="col-md-9">
				<input type="text" class="form-control" name="name" value="" required autofocus />
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-3 control-label">{{utrans('contact.email')}}<sup>*</sup></label>
				<div class="col-md-9">
				<input type="email" class="form-control" name="email" value="" required />
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-3 control-label">{{utrans('contact.object')}}</label>
				<div class="col-md-9">
				<input type="text" class="form-control" name="phone" value="" />
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-md-3 control-label">{{utrans('contact.message')}}<sup>*</sup></label>
				<div class="col-md-9">
				<textarea type="text" class="form-control" name="message" value="" required style="resize:vertical"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<button type="submit" class="btn btn-primary">{{utrans('contact.send')}}</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop