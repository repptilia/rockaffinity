@section('page')

<p class="lead">{{utrans("$type.edit")}}</p>
<form class="form-horizontal ra_form_edit" data-type="{{$type}}" data-id="{{$item->id}}">
@foreach ( $item->getFillable() as $attr )
	<div class="form-group">
		<label for="ra_edit_{{$attr}}" class="col-sm-2 control-label">{{utrans("$type.$attr")}}</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="ra_edit_{{$attr}}" data-name="{{$attr}}" value="{{$item->getOriginal()[$attr]}}" />
		</div>
	</div>
@endforeach
</form>
@stop
