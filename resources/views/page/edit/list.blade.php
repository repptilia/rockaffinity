@section('page')
<p class="lead">{{utrans("$type.list")}}</p>
<div class="row">
@foreach ( $items as $item )
	<a href="{{action('HomeController@getEdit',[$type,$item->id])}}">{{$item->title?:$item->name}}</a><br />
@endforeach
</div>
@stop