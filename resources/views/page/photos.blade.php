@section('page')
<p class="lead">{{$album['name']}}<a href="{{action('MainController@getPhotos')}}" class="pull-right">{{utrans('album.list')}}</a></p>
<div class="row">
@foreach ( $album['data'] as $photo )
	<div class="col-xs-6" style="text-align:center;margin-bottom:10px">
			<img class="img-responsive" src="http://graph.facebook.com/{{$photo->id}}/picture" alt="" />
	</div>
@endforeach
</div>
@stop