@section('page')
<p class="lead">On Spotify:</p>
@foreach($spotifySongs as $song)
	{{$song->title}} - {{$song->popularity}}<br />
@endforeach
<p class="lead">User liked:</p>
@foreach($likedSongs as $song)
	{{$song->title}} - {{$song->likes}}<br />
@endforeach
@stop