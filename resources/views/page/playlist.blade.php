@section('page')
<p class="lead">{{$playlist->name}}<a href="{{action('MainController@getPlaylists')}}" class="pull-right">{{utrans('playlist.backToList')}}</a></p>
<div class="row">
@foreach ( $playlist->tracks as $track )
	<div class="col-sm-3 col-xs-6" style="text-align:center;">
		<a href="{{$track->link}}">
			<span class="text-align:center">
@for ( $i=0; $i < $track->hearts ; $i ++ )
<span class="glyphicon glyphicon-heart"></span>
@endfor
			</span><br />
			<img src="{{$track->cover}}" alt="" class="img-thumbnail" style="height:100px;width:100px" />
			<p style="white-space:nowrap;overflow:hidden;text-overflow:ellipsis;">{{$track->title}} - {{$track->artist}}</p>
		</a>
	</div>
@endforeach
</div>
@stop