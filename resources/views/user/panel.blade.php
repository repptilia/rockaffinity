<div class="panel panel-default">
	<div class="panel-body">
{{utrans('user.welcome',['name' => Auth::user()->name])}}
	<a href="{{action('Auth\AuthController@getLogout')}}" class="text-danger pull-right"><span class="glyphicon glyphicon-off"></span> {{utrans('auth.logout')}}</a>
	</div>
</div>
@if ( isset($breadcrumbs) )
<ol class="breadcrumb">
	<li><a href="{{action('HomeController@getDashboard')}}">{{utrans('user.dashboard')}}</a></li>
	@foreach ( $breadcrumbs as $breadcrumb )
		@if ( $breadcrumb !== end($breadcrumb) )
			<li>
		@else
			<li class="active">
		@endif
				<a href="{{$breadcrumb[0]}}">{{$breadcrumb[1]}}</a>
			</li>
	@endforeach
</ol>
@else
<ol class="breadcrumb">
	<li class="active"><a href="{{action('HomeController@getDashboard')}}">{{utrans('user.dashboard')}}</a></li>
</ol>
@endif
{{--
<ol class="breadcrumb">
	<li><a href="{{action('HomeController@getDashboard')}}">{{utrans('user.dashboard')}}</a></li>
	<li><a href="#">Library</a></li>
	<li class="active">Data</li>
</ol>
--}}