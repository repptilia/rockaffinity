@section('page')
<div id="event-{{$event->id}}" class="ra_hash"></div><p class="lead">{{$event->name}} <span class="pull-right">{{$event->time}}</span></p>
<div class="row">
	<div class="col-md-4">
		<iframe style="width:100%;border:0" height="200" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA6KyHsATuHdj2ufLXz8P5q03OOcaBeqR8&q={{urlencode($event->location)}}"></iframe>
	</div>
	<div class="col-md-8" style="max-height:250px;overflow:hidden;text-overflow:ellipsis">
		{!!nl2br($event->description)!!}
	</div>
</div>
<img src="{{$event->picture}}" alt="" class="img-responsive" />

@stop