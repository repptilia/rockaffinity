<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a href="{{route('index')}}" class="navbar-brand hidden-md hidden-sm hidden-xs"><img src="{{asset('image/LogoNoText.png')}}" style="height:60px" alt="" /></a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ra_nav">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="ra_nav">
			<ul class="nav navbar-nav">
@foreach ( Config::get('nav.list',[]) as $nav_item => $item )
	@if ( ! is_array($item) )
		@if ( Session::get('nav.page' === $nav_item) )
				<li class="active">
		@else
				<li>
		@endif
					<a href="{{action($item)}}">{{utrans("nav.$nav_item")}}</a>
				</li>
	@else
		@if ( starts_with(Session::get('nav.page'),$nav_item) )
				<li class="dropdown active">
		@else
				<li class="dropdown">
		@endif
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{utrans("nav.$nav_item")}} <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
		@foreach ( $item as $s_nav_item => $s_item )
			@if ( Session::get('nav.page') === "$nav_item.$s_nav_item" )
						<li class="active">
			@else
						<li>
			@endif
							<a href="{{action($s_item)}}">{{utrans("nav.$nav_item.$s_nav_item")}}</a>
						</li>
		@endforeach
				</ul>
			</li>
	@endif
@endforeach
			</ul>
@include('item.languageBar')
		</div>
	</div>
</nav>