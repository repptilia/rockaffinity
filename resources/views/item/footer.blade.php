<div id="ra_footer" class="hidden-xs navbar-inverse">
	<div class="container">
		<div class="navbar-left">
			<a href="http://epfl.ch"><img src="{{asset('image/sp/EPFL.png')}}" alt="" class="ra_footer_img" /></a>
			<a href="http://agepoly.epfl.ch"><img src="{{asset('image/sp/logo_agepoly.png')}}" alt="" class="ra_footer_img" /></a>
			<a href="http://agepoly.epfl.ch"><img src="{{asset('image/DanceSquare.png')}}" alt="" class="ra_footer_img" /></a>
		</div>
		<div class="navbar-right">
			{{--<iframe class="navbar-text" src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fchillingmonkey&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20&amp;appId=1564737543750140" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:20px;color:#FFF" allowTransparency="true"></iframe>--}}
@if ( ! Auth::check() )
			<a href="{{action('Auth\AuthController@getFb')}}" class="btn navbar-btn btn-primary">{{utrans('auth.login')}}</a>
@endif
			<p class="navbar-text">Created by Etienne</p>
		</div>
	</div>
</div>