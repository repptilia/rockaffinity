<ul id="navlg" class="nav navbar-nav navbar-right hidden-xs">
	<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{utrans('lang.'.App::getLocale())}} <span class="caret"></span></a>
		<ul id="language" class="dropdown-menu" role="menu">
@foreach(Config::get('lang.list',[]) as $lg => $language)
	@if ( $lg !== App::getLocale() )
			<li>
				<a data-lg="{{$lg}}">{{utrans("lang.$lg")}}</a>
			</li>
	@endif
@endforeach
		</ul>
	</li>
</ul>
