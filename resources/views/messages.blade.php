@foreach( $errors->all() as $f_message )
<script>ra_an("{{$f_message}}",false)</script>
@endforeach

@foreach( Session::pull('messages.errors',array()) as $f_message )
<script>ra_an("{{$f_message}}",false)</script>
@endforeach

@foreach( Session::pull('messages.valids',[]) as $f_message )
<script>ra_ap("{{$f_message}}",false)</script>
@endforeach