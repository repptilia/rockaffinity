<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="EPFL dance association!" />
		<meta name="keywords" content="EPFL,epfl,dance,rock,tacos,bar,tacos bar,acrobatic,events" />
		<meta name="ra_token" content="{{csrf_token()}}" />
		<meta name="ra_locale" content="{{App::getLocale()}}" />
		<link rel="stylesheet" href="{{asset(elixir('css/ra.css'))}}" />
		<link rel="shortcut icon" type="image/x-icon" href="{{asset('image/favicon.ico')}}" />
@yield('metalist')
		<title>RockAffinity</title>
	</head>
	<body>
@include('item.header')
		<div class="container">
			<div class="row">
				<div class="col-md-offset-1 col-md-10 col-xs-12">
					<div id="ra_alerts"></div>
@if ( Auth::check() )
@include('user.panel')
@endif
@yield('page')
@show
				</div>
			</div>
			<div style="height:10px"></div>
		</div>
@include('item.footer')
		<script type="text/javascript">
var folder = '{{url()}}/';
		</script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="{{asset(elixir('js/ra.js'))}}"></script>
		<div id="fb-root"></div>
	</body>
</html>
@section('messages')
@include('messages')
@show
