function ra_sl(e) {
    e = typeof e != "undefined" ? e : 750, ra_l = !0, setTimeout(function() {
        ra_l && !$("ra_jq_loader").length && !$("ra_jq_loader_screen").length && $("body").after('<div id="ra_jq_loader_screen"></div><div id="ra_jq_loader"><img src="' + folder + 'image/_loader.gif" class="m-center" /><div id="ra_jq_text">Loading</div></div>'), ra_l = !1
    }, e)
}

function ra_hl() {
    ra_l = !1, $("#ra_jq_loader").remove(), $("#ra_jq_loader_screen").remove()
}

/**
 * Dialogs with the server.
 *
 * @param int a 
 */
function ra_dialog(a,b,c,d){
    // Add token to form
    var token=$('meta[name="ra_token"]').prop('content');

    if ( typeof token == "undefined" ) return;

    if ( typeof b === 'string' )
        b += '&_token'+token;

    else
        b._token=token;
    // Show loader
    ra_sl(d);
    if ( a === null)
        a = document.URL;
    else
        a = folder+a;
    var jqxhr = $.post(a,b,function(a){
        try
        {
            ra_c();
            for (i = 0; i < a.messages.valids.length; i++) ra_ap(a.messages.valids[i], !1);
            for (i = 0; i < a.messages.errors.length; i++) ra_an(a.messages.errors[i], !1);

            if ( typeof c != "undefined" && a.success )
                c(a.data);
        }
        catch(e){
            //console.log(e);
        }
    },"json")
    .always(function(){ra_hl(),ra_c})
    .fail(function(j,t,e){console.log(j.responseText)});
}

function ra_c() {
    $("#ra_alerts .alert-dismissible").remove()
}

function ra_ap(e, t) {
    t = typeof t != "undefined" ? t : !0, t && ra_c(), $("#ra_alerts").append('<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + e + "</div>"), $("#ra_alerts .alert-dismissible").delay(4e3).fadeOut(200)
}

function ra_an(e, t) {
    t = typeof t != "undefined" ? t : !0, t && ra_c(), $("#ra_alerts").append('<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + e + "</div>"), $("#ra_alerts .alert-dismissible").delay(4e3).fadeOut(200)
}

var ra_l = !1;

$(function(){
    // Set width of alert box
    $("#ra_alerts").css("width", $("#ra_alerts").parent().width());

    // Set language
    $(document).on('click','#language li',function(){
        ra_dialog('lang/set-language',
            {lg:$(this).find('a').data('lg')},
            function(r){
                location.reload()
            })
    });

    // Update profile
    $(document).on('change','.ra_profile_edit input, .ra_profile_edit textarea',function(){
        ra_dialog('my/profile',{
            name:$(this).data('name'),
            value:$(this).prop('value'),
            id:$('.ra_profile_edit').data('id')
        });
    });

    // Update generic
    $(document).on('change','.ra_form_edit input, .ra_form_edit textarea',function(){
        ra_dialog('my/edit',{
            name:$(this).data('name'),
            value:$(this).prop('value'),
            id:$('.ra_form_edit').data('id'),
            type:$('.ra_form_edit').data('type'),
        });
    });

    var ra_locale = $('meta[name="ra_locale"]').prop('content');

    if($('#ra_calendar').length){
        var ra_data={
            tmpl_path: folder+"tmpls/",
            events_source: $.parseJSON($('#ra_calendar_data').html()),
            view:'month',
            onAfterViewLoad: function(view) {
                $('#ra_cal_title').text(this.getTitle());
            },
            views:
            {
                year:
                {
                    enable: 1
                },
                month:
                {
                    enable: 1
                },
                week:
                {
                    enable: 0
                },
                day:
                {
                    enable: 0
                }
            },
            onBeforeEventsLoad:function(next){
                // Fix for events between
                this.getEventsBetween = function(start, end) {
                    var events = [];
                    $.each(this.options.events, function() {
                        if(this.start == null) {
                            return true;
                        }
                        var event_end = this.end || this.start;
                        if((parseInt(this.start) < end) && (parseInt(event_end) > start)) {
                            events.push(this);
                        }
                    });
                    return events;
                };

                    function getExtentedOption(cal, option_name) {
        var fromOptions = (cal.options[option_name] != null) ? cal.options[option_name] : null;
        var fromLanguage = (cal.locale[option_name] != null) ? cal.locale[option_name] : null;
        if((option_name == 'holidays') && cal.options.merge_holidays) {
            var holidays = {};
            $.extend(true, holidays, fromLanguage ? fromLanguage : defaults_extended.holidays);
            if(fromOptions) {
                $.extend(true, holidays, fromOptions);
            }
            return holidays;
        }
        else {
            if(fromOptions != null) {
                return fromOptions;
            }
            if(fromLanguage != null) {
                return fromLanguage;
            }
            return defaults_extended[option_name];
        }
    }


                // Fix for day
                this._day = function(week, day) {
                    this._loadTemplate('month-day');

                    var t = {tooltip: '', cal: this};
                    var cls = this.options.classes.months.outmonth;

                    var firstday = this.options.position.start.getDay();
                    if(getExtentedOption(this, 'first_day') == 2) {
                        firstday++;
                    } else {
                        firstday = (firstday == 0 ? 7 : firstday);
                    }

                    day = (day - firstday) + 1;
                    var curdate = new Date(Date.UTC(this.options.position.start.getFullYear(), this.options.position.start.getMonth(), day, 0, 0, 0));

                    // if day of the current month
                    if(day > 0) {
                        cls = this.options.classes.months.inmonth;
                    }
                    // stop cycling table rows;
                    var daysinmonth = (new Date(this.options.position.end.getTime() - 1)).getDate();
                    if((day + 1) > daysinmonth) {
                        this.stop_cycling = true;
                    }
                    // if day of the next month
                    if(day > daysinmonth) {
                        day = day - daysinmonth;
                        cls = this.options.classes.months.outmonth;
                    }

                    cls = $.trim(cls + " " + this._getDayClass("months", curdate));

                    if(day <= 0) {
                        var daysinprevmonth = (new Date(this.options.position.start.getFullYear(), this.options.position.start.getMonth(), 0)).getDate();
                        day = daysinprevmonth - Math.abs(day);
                        cls += ' cal-month-first-row';
                    }

                    var holiday = this._getHoliday(curdate);
                    if(holiday !== false) {
                        t.tooltip = holiday;
                    }

                    t.data_day = curdate.getFullYear() + '-' + curdate.getMonthFormatted() + '-' + (day < 10 ? '0' + day : day);
                    t.cls = cls;
                    t.day = day;

                    t.start = parseInt(curdate.getTime());
                    t.end = parseInt(t.start + 86400000);
                    t.events = this.getEventsBetween(t.start, t.end);
                    return this.options.templates['month-day'](t);
                }

                next();
            }
        };
        if(ra_locale=='fr'){
            ra_data.language='fr-FR';
        }

        // Set calendar
        var calendar = $("#ra_calendar").calendar(ra_data);

        $('.btn-group button[data-calendar-nav]').each(function() {
            var $this = $(this);
            $this.click(function() {
                calendar.navigate($this.data('calendar-nav'));
            });
        });
    }

    // Send contact
    $(document).on('click','#ra_contact_me',function(){
        ra_dialog(null,{message:$('#ra_contact_msg').prop('value')},function(){});
            $('#ra_contactM').modal('hide');
    });
});

(function(e, t, n) {
        var r, i = e.getElementsByTagName(t)[0];
        if (e.getElementById(n)) return;
        r = e.createElement(t), r.id = n, r.src = "//connect.facebook.net/en_US/sdk.js", i.parentNode.insertBefore(r, i)
    })(document, "script", "facebook-jssdk");

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-54844287-1', 'auto');
ga('send', 'pageview');