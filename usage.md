# Connexion #

1. La connexion se fait via le bouton "connecter" dans le footer.
1. Seules les personnes qui peuvent éditer du contenu sur la page Rock Affinity peuvent se connecter au site.

# Mon Profil #

1. Laisser le champ "role" tel quel, il est traduit proprement par la suite
1. Précéder une ligne de * transforme la ligne en titre.
1. Pour ajouter des "smileys", vous pouvez insérer <span class="glyphicon glyphicon-asterisk"></span>, remplaçant asterisk par quelque chose d'autre, dont vous pouvez trouver la liste complète sur [bootstrap](http://getbootstrap.com/components/#glyphicons)
1. Voici une liste des questions auxquelles vous pouvez répondre: (copiez-collez le code ci-dessous) [aperçu](http://dancesquare.epfl.ch/RockAffinity/rocker/etienne)

```
*Les études, ça se passe ? qu'est ce que tu fais ?
reponse

*T’es vieux comment ?
reponse<span class="glyphicon glyphicon-user"></span>

*Tu danses le rock depuis combien de temps ?
reponse

*Qu'est ce qui t'as amené à danser ?
reponse

*Les autres danses, ça te parle ? t'as testé ? tu pratiques ?
reponse

*Et en dehors de la danse ? qu'est que tu fais de ton temps libre ?
reponse

*Ton meilleur souvenir rock'n'rollesque ? 
reponse

*Ketchup ou mayo ?
reponse
*Blonde ou brune ?
reponse
*Bière ou vin ?
reponse
*Karmeliet ou Kwak ?
reponse
*iPhone ou Android ?
reponse
*Viande ou poisson ?
reponse
*Lac ou montagne ?
reponse
*Chien ou chat ?
reponse
*Rouge ou blanc ?
reponse
*Fondue ou raclette ?
reponse
*Migros ou Coop ?
reponse
```

# Evènements #

1. Tous les autres champs sont remplacés par ce qui vient de Facebook (fait chaque minute), à l'exception du lien.
2. A venir
